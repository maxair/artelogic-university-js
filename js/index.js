import * as helper from './modules/helper.js';

const progressBtn = document.querySelector('.start-menu__progress-button');
const listElem = document.querySelector('.start-menu__list');
const modalElem = document.querySelector('.modal');
const modalBodyElem = document.querySelector('.modal__body');
const overlayElem = document.querySelector('.overlay');
const resultElem = document.querySelector('.result');

const progress = helper.getItem('progress', true);

/*
 * Functions
 */
const attachProgress = () => {
  if (!progress) {
    return;
  }

  progressBtn.classList.remove('button--disabled');

  for (let [i, attempt] of progress.entries()) {
    const numOfQuestions = helper.getItem('numOfQuestions');
    const listItem = helper.createElement('li', 'start-menu__item');
    const attemptNumber = helper.createElement('span', 'start-menu__item-num');
    const attemptText = document.createTextNode(
        `Result: ${attempt.score} of ${numOfQuestions}`,
    );

    listItem.dataset.attempt = attemptNumber.textContent = i + 1;
    listItem.addEventListener('click', (e) => displayResult(e));

    listItem.append(attemptNumber, attemptText);
    listElem.appendChild(listItem);
  }
};

const displayResult = (e) => {
  const selectedAttempt = e.currentTarget;
  const attemptIndex = parseInt(selectedAttempt.dataset['attempt']) - 1;
  const userAnswers = progress[attemptIndex].answers;

  helper
      .loadQuestions()
      .then((loadedQuestions) => {
        renderResult(loadedQuestions, userAnswers);
        openModal();
      })
    .catch((error) => alert(error));
};

const renderResult = (questions, userAnswers) => {
  resultElem.innerHTML = '';

  for (const [questionIndex, question] of questions.entries()) {
    const resultRowElem = helper.createElement('div', 'result__row');
    const resultRowTitleElem = helper.createElement('h3', 'result__row-title');
    const questionBoxElem = helper.createElement('div', 'questions-box');
    const questionElem = helper.createElement('h2', 'questions-box__question');

    const correctAnswer = question.answer - 1;
    const userAnswer = userAnswers[questionIndex] - 1;
    const choicesElem = createResultChoices(
        question,
        correctAnswer,
        userAnswer,
    );

    resultRowTitleElem.textContent = `Question: ${questionIndex + 1}`;
    questionElem.textContent = question.question;

    questionBoxElem.append(questionElem, choicesElem);
    resultRowElem.append(resultRowTitleElem, questionBoxElem);
    resultElem.appendChild(resultRowElem);
  }
};

const createResultChoices = (question, correctAnswer, userAnswer) => {
  const choicesElem = helper.createElement('div', 'questions-box__choices');

  for (const [choiceIndex, choice] of question.choices.entries()) {
    const choiceElem = helper.createChoice(choiceIndex, choice);

    if (choiceIndex === correctAnswer) {
      choiceElem.classList.add('questions-box__choice--correct');
    }
    if (choiceIndex === userAnswer && correctAnswer !== userAnswer) {
      choiceElem.classList.add('questions-box__choice--incorrect');
    }

    choicesElem.classList.add('questions-box__choice--disabled');
    choicesElem.append(choiceElem);
  }

  return choicesElem;
};

const expandProgress = (e) => {
  e.target.classList.toggle('start-menu__progress-button--active');
  listElem.classList.toggle('start-menu__list--active');
};

const openModal = () => {
  modalElem.classList.remove('hidden');
  overlayElem.classList.remove('hidden');
};

const closeModal = () => {
  modalBodyElem.scrollTop = 0;
  modalElem.classList.add('hidden');
  overlayElem.classList.add('hidden');
};

/*
 * Events
 */
document.addEventListener('click', (e) => {
  const targetId = e.target.id;

  switch (targetId) {
    case 'progress-btn':
      expandProgress(e);
      break;
    case 'close-btn':
    case 'overlay':
      closeModal();
      break;
  }
});

document.addEventListener('keydown', (e) => {
  if (e.key === 'Escape' && !modalElem.classList.contains('hidden')) {
    closeModal();
  }
});

/*
 * Execution
 */
attachProgress();
