const loadQuestions = async () => {
  const response = await fetch('quiz.json');

  if (!response.ok) {
    throw new Error(`An error has occured: ${response.status}`);
  }

  return await response.json();
};

const createChoice = (choiceIndex, choice) => {
  const characters = 'abcdefghijklmnopqrstuvwxyz';
  const choiceElem = createElement('div', 'questions-box__choice');
  const choicePrefixElem = createElement('p', 'questions-box__choice-prefix');
  const choiceTextElem = createElement('p', 'questions-box__choice-text');

  choicePrefixElem.textContent = characters.charAt(choiceIndex);
  choiceTextElem.textContent = choice;
  choiceElem.append(choicePrefixElem, choiceTextElem);

  return choiceElem;
};

const createElement = (tagName, className = '') => {
  const element = document.createElement(tagName);
  element.className = className;

  return element;
};

const getItem = (key, parse = false) => {
  const item = localStorage.getItem(key);

  return parse ? JSON.parse(item) : item;
}

const setItem = (key, value, stringify = false) => {
  localStorage.setItem(key, stringify ? JSON.stringify(value) : value);
}

export {loadQuestions, createChoice, createElement, getItem, setItem};
