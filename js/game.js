import * as helper from './modules/helper.js';

const gameElem = document.querySelector('.game');
const questionElem = document.querySelector('.questions-box__question');
const choicesElem = document.querySelector('.questions-box__choices');
const counterElem = document.getElementById('counter');
const scoreElem = document.getElementById('score');

let questions,
  userAnswers,
  score,
  currentQuestion,
  questionCounter,
  isAnswerSelected;

/*
 * Functions
 */
const startGame = (loadedQuestions) => {
  questions = loadedQuestions;
  userAnswers = [];
  score = 0;
  questionCounter = 0;
  currentQuestion = {};

  getQuestion();
  gameElem.classList.add('game--visible');
};

const getQuestion = () => {
  if (questionCounter === questions.length) {
    return endGame();
  }

  isAnswerSelected = false;
  currentQuestion = questions[questionCounter++];

  counterElem.textContent = `${questionCounter}/${questions.length}`;
  questionElem.textContent = currentQuestion.question;

  renderChoices();
};

const renderChoices = () => {
  choicesElem.innerHTML = '';

  for (const [i, choice] of currentQuestion.choices.entries()) {
    const choiceElem = helper.createChoice(i, choice);

    choiceElem.dataset.number = i + 1;
    choiceElem.addEventListener('click', (e) => checkAnswer(e));

    choicesElem.append(choiceElem);
  }
};

const checkAnswer = (e) => {
  if (isAnswerSelected) {
    return;
  }

  const selectedChoice = e.currentTarget;
  const selectedAnswer = parseInt(selectedChoice.dataset['number']);
  let classToApply = 'questions-box__choice--incorrect';

  isAnswerSelected = true;
  userAnswers.push(selectedAnswer);

  if (selectedAnswer === currentQuestion.answer) {
    classToApply = 'questions-box__choice--correct';
    scoreElem.textContent = ++score;
  }

  selectedChoice.classList.add(classToApply);
  setTimeout(() => getQuestion(), 1000);
};

const endGame = () => {
  const progress = helper.getItem('progress', true) || [];
  const attempt = {
    answers: userAnswers,
    score: score,
  };

  progress.push(attempt);

  helper.setItem('mostRecentScore', score);
  helper.setItem('numOfQuestions', questions.length);
  helper.setItem('progress', progress, true);

  window.location.assign('end.html');
};

/*
 * Execution
 */
helper.loadQuestions()
    .then((loadedQuestions) => {
      startGame(loadedQuestions);
    })
  .catch((error) => {
    alert(error);
    window.location.assign('index.html');
  });
